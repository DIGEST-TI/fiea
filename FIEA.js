﻿var FIEA = {

    init: function () {
        this.setEvents();
    },

    setEvents: function () { },

    campoShow: function (fieldID) {
        var field = $(fieldID);
        field.show();
        field[0].setAttribute('required', 'S');

        return field;
    },
    campoHide: function (fieldID) {
        var field = $(fieldID);
        field.hide();
        field[0].setAttribute('required', 'N');

        return field;
    },

    hideNsHow: function (hide, show) {
        try {
            //esconde
            $.each(hide, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).hide();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', 'N');
                        // $(this).val("");
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', 'N');
                    $('inp:' + v).val("");
                    if ($('inp:' + v).closest("tr").is(":visible") || $('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").hide();
                    }
                }
            });

            //mostra
            $.each(show, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).show();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', 'S');
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', 'S');
                    if (!$('inp:' + v).closest("tr").is(":visible") || !$('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").show();
                    }
                }
            });
        } catch (e) {
            console.error(e);
        }
    },
    hideNsHowRequiredN: function (hide, show) {
        try {
            //esconde
            $.each(hide, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).hide();
                } else {
                    $('inp:' + v).val("");
                    if ($('inp:' + v).closest("tr").is(":visible") || $('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").hide();
                    }
                }
            });

            //mostra
            $.each(show, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).show();
                } else {
                    if (!$('inp:' + v).closest("tr").is(":visible") || !$('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").show();
                    }
                }
            });
        } catch (e) {
            console.error(e);
        }
    },
    hideNsHowClass: function (hide, show) {
        try {
            //esconde
            $.each(hide, function (k, v) {
                if (v.indexOf(".") == 0) {
                    $(v).hide();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', 'N');
                        //$(this).val("");
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', 'N');
                    $('inp:' + v).val("");
                    if ($('inp:' + v).closest("tr").is(":visible") || $('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").hide();
                    }
                }
            });

            //mostra
            $.each(show, function (k, v) {
                if (v.indexOf(".") == 0) {
                    $(v).show();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', 'S');
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', 'S');
                    if (!$('inp:' + v).closest("tr").is(":visible") || !$('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").show();
                    }
                }
            });
        } catch (e) {
            console.error(e);
        }
    },
    loadFields: function (obj) {
        // Campos do formulário
        Object.keys(obj)
            .forEach(field => {
                //
                if (field.includes("inpModal")) {
                    obj[field] = $('#' + field.replace('Modal', ''))
                } else if (!field.includes("inpModal") && field.includes("inp")) {//inpNomeDoCampo
                    obj[field] = $(field.replace(field.charAt(3), ':' + field.charAt(3).toLowerCase()))
                } else if (field.includes("btn")) {
                    obj[field] = $(field.replace('btn', '#btn'))
                } else if (field.includes("class")) {//classNomeDoCampo
                    obj[field] = '.' + field.charAt(5).toLowerCase() + field.substr(6)
                } else if (field.includes("dataId")) {//dataIdNomeDoCampo
                    obj[field] = '#' + field.charAt(6).toLowerCase() + field.substr(7)
                }
            })
    },
    hideElements: function (objArr) {
        //[obj,obj,obj,...]
        $.each(objArr, function (i, e) {
            if ($(e)[0].getAttribute('required') == 'S') {
                $(e)[0].setAttribute('required', 'N')
            }
            $(e).parent().hide();
        });
    },
    showElements: function (objArr) {
        //[{"element":obj,required:true},{"element":obj,required:false},...]
        $.each(objArr, function (i, e) {
            e.required ? e.element[0].setAttribute('required', 'S') : e.element[0].setAttribute('required', 'N')
            $(e.element).parent().show();
        });
    },
    stringToFloat(obj) {
        if (obj != null && obj != "") {
            obj = obj.toString().replace(/[.]/g, '')
            obj = obj.toString().replace(',', '.')
            obj = parseFloat(obj)
            return obj
        } else {
            return 0
        }
    },
    floatToString(obj) {
        if (obj != null && obj != "") {
            obj = obj.toFixed(2).toString().replace(/[.]/g, ',')
            obj = obj.toString().replace(/\d(?=(?:\d{3})+(?:\D|$))/g, "$&.")
            return obj
        } else {
            return '0,00'
        }
    },

    // Visibilidade dos campos(Por Id ou Classe) e obrigatoriedade(Quando visivel)
    visibilidadeCampo: function (arrEsconde, arrMostra, isRequired) {
        try {
            //esconde
            $.each(arrEsconde, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).hide();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', ((isRequired == true) ? 'S' : 'N'));
                    });
                } else if (v.indexOf(".") == 0) {
                    $(v).hide();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', ((isRequired == true) ? 'S' : 'N'));
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', ((isRequired == true) ? 'S' : 'N'));
                    $('inp:' + v).val("");
                    if ($('inp:' + v).closest("tr").is(":visible") || $('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").hide();
                    }
                }
            });

            //mostra
            $.each(arrMostra, function (k, v) {
                if (v.indexOf("#") == 0) {
                    $(v).show();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', ((isRequired == false) ? 'N' : 'S'));
                    });
                } else if (v.indexOf(".") == 0) {
                    $(v).show();
                    $(v).find("[xname^='inp']").each(function () {
                        this.setAttribute('required', ((isRequired == false) ? 'N' : 'S'));
                    });
                } else {
                    $('inp:' + v)[0].setAttribute('required', ((isRequired == false) ? 'N' : 'S'));
                    if (!$('inp:' + v).closest("tr").is(":visible") || !$('inp:' + v).is(":visible")) {
                        $('inp:' + v).closest("tr").show();
                    }
                }
            });
        } catch (e) {
            console.error(e);
        }
    },
    limpaTabela: function (element) {
        let tables = $(element)
            .find('tr')
            .not('tr.header')
            .toArray()
        tables.forEach(function (elem, index) {
            if (index + 1 == tables.length) {
                $(element)
                    .find('button#BtnInsertNewRow')
                    .trigger('click')
            }
            $(elem).remove()
        })
    }
}

$(window).load(function () {
    FIEA.init();
});